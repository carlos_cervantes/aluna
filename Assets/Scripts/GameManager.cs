﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	public class GameManager : MonoBehaviour
	{
		public static GameManager Instance;

		void Awake ()
		{
			Instance = this;
		}

		[SerializeField]
		public InventoryContent playerInventory = new InventoryContent ();

		[SerializeField]
		float _MaxStamina = 20.0f;

		public static float MaxStamina {
			get {
				return Instance._MaxStamina;
			}
			set {
				Instance._MaxStamina = value;
				Instance._CurrentStamina = Mathf.Clamp (Instance._CurrentStamina, 0.0f, Instance._MaxStamina);
			}
		}

		[SerializeField]
		float _CurrentStamina = 20.0f;

		public static float CurrentStamina {
			get {
				return Instance._CurrentStamina;
			}
			set {
				Instance._CurrentStamina = Mathf.Clamp (value, 0.0f, MaxStamina);
			}
		}


		public static InventoryContent Inventory {
			get {
				return Instance.playerInventory;
			}
		}
	}
}