﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	public class ChangeStamina : Effect
	{

		public float Change = 0.0f;

		public override void Apply (GameObject contextSubject, EventType evt, GameObject contextObject)
		{
			GameManager.CurrentStamina += Change;
		}
	}
}