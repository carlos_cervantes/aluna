﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	[System.Serializable]
	public class AudioEffect : Effect
	{

		public AudioClip clip;

		public override void Apply (GameObject contextSubject, EventType evt, GameObject contextObject)
		{
			AudioSource.PlayClipAtPoint (clip, contextObject.transform.position);
		}

	}
}