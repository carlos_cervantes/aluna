﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Aluna
{
	[CustomPropertyDrawer (typeof(TriggeredEffect))]
	public class TriggerdEffectPropertyDrawer : PropertyDrawer
	{

		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			return EditorGUIUtility.singleLineHeight;
		}

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty (position, label, property);
			Rect TriggerRect = position;
			TriggerRect.width = 0.5f * position.width;
			Rect EffectRect = position;
			EffectRect.width = 0.5f * position.width;
			EffectRect.x += TriggerRect.width;

			EditorGUI.PropertyField (TriggerRect, property.FindPropertyRelative ("Trigger"), GUIContent.none);
			EditorGUI.PropertyField (EffectRect, property.FindPropertyRelative ("Effect"), GUIContent.none);

			EditorGUI.EndProperty ();
		}
	}
}