﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.Tilemaps;

public class WorldBuilder : EditorWindow
{

	static WorldBuilder _window;

	public static WorldBuilder window {
		get {
			if (_window == null)
				_window = GetWindow<WorldBuilder> ();
			return _window;
		}
	}


	[MenuItem ("World Builder/Editor")]
	public static void Init ()
	{
		_window = GetWindow<WorldBuilder> ();
		_window.Show ();
	}


	void OnGUI ()
	{


		PlaceableOverview ();

	}




	#region Placeables

	GameObject _SelectedPlaceable;

	GameObject SelectedPlaceable {
		get {
			return _SelectedPlaceable;
		}
		set {
			if (_SelectedPlaceable == null && value != null) {
				SceneView.onSceneGUIDelegate -= HandlePlacingInScene;
				SceneView.onSceneGUIDelegate += HandlePlacingInScene;
			} else if (_SelectedPlaceable != null && value == null) {
				SceneView.onSceneGUIDelegate -= HandlePlacingInScene;
			}
			_SelectedPlaceable = value;
		}
	}

	void PlaceableOverview ()
	{
		if (GUILayout.Button ("Reground")) {
			ReGround ();
		}

		EditorGUILayout.BeginHorizontal ();
		string[] PlaceableAssets = AssetDatabase.FindAssets ("t:GameObject", new string[] { "Assets/Prefabs/Placeables" });
		int i = 0;
		foreach (string guid in PlaceableAssets) {
			GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject> (AssetDatabase.GUIDToAssetPath (guid));
			Texture icon = AssetPreview.GetAssetPreview (obj);
			Color backupColor = GUI.backgroundColor;
			if (obj == SelectedPlaceable)
				GUI.backgroundColor = Color.yellow;
			if (GUILayout.Button (icon, GUILayout.Width (75.0f), GUILayout.Height (75.0f))) {
				if (obj == SelectedPlaceable)
					SelectedPlaceable = null;
				else
					SelectedPlaceable = obj;
				Tools.current = Tool.None;
			}
			GUI.backgroundColor = backupColor;
		}
		EditorGUILayout.EndHorizontal ();
	}


	void ReGround ()
	{
		foreach (GameObject root in EditorSceneManager.GetActiveScene().GetRootGameObjects()) {
			foreach (IPlaceable placeable in root.GetComponentsInChildren<IPlaceable>(true)) {
				placeable.Ground ();
			}
		}
	}

	Plane GroundPlane = new Plane (Vector3.up, Vector3.zero);

	Tilemap _tilemap;

	Tilemap tilemap {
		get {
			if (_tilemap == null) {
				_tilemap = GameObject.FindObjectOfType<Tilemap> ();
			}
			return _tilemap;
		}
	}

	void HandlePlacingInScene (SceneView sceneView)
	{
		Event e = Event.current;
		if (e.type == EventType.MouseDown && e.button == 0) {
			Debug.Log ("MOUSE DOWN");
			Ray ray = HandleUtility.GUIPointToWorldRay (e.mousePosition);
			#if true
			Debug.DrawRay (ray.origin, ray.direction, Color.red, 30.0f);
			Debug.Log ("Ray " + ray);
			float distance = 0.0f;
			if (GroundPlane.Raycast (ray, out distance)) {
				Vector3 point = ray.origin + distance * ray.direction;
				Debug.Log ("HP " + point);
				Debug.DrawLine (point, point + 1000.0f * Vector3.up);
				Vector3Int gpos = tilemap.layoutGrid.WorldToCell (point);
				Debug.Log ("gPos " + gpos);
				TileBase tb = tilemap.GetTile (gpos);
				if (tb == null) {
					return;
				}
				Debug.Log ("ttype " + tb.GetType ());
				GameObject placed = (GameObject)PrefabUtility.InstantiatePrefab (SelectedPlaceable, tilemap.gameObject.scene);
				placed.transform.position = point;
				IPlaceable placeable = placed.GetComponent<IPlaceable> ();
				if (placeable != null) {
					Debug.Log ("PLACEABLE");
					Transform parentObj = tilemap.layoutGrid.transform.Find (placeable.path);
					if (parentObj == null) {
						Debug.Log ("CREATE GROUP");
						GameObject newParentObj = new GameObject (placeable.path);
						parentObj = newParentObj.transform;
						parentObj.SetParent (tilemap.layoutGrid.transform);
					}
					placed.transform.SetParent (parentObj.transform, true);
					EditorSceneManager.MarkSceneDirty (parentObj.gameObject.scene);

					placeable.Randomize ();
					Selection.activeGameObject = null;
				}
				e.Use ();	
			}

			#else
			
			RaycastHit rayHit;

			if (Physics.Raycast (ray, out rayHit)) {
				Debug.Log ("HIT");
				PlacingGround ground = rayHit.collider.gameObject.GetComponent<PlacingGround> ();
				if (ground == null)
					return;
				Debug.Log ("GROUND");
				GameObject placed = (GameObject)PrefabUtility.InstantiatePrefab (SelectedPlaceable, rayHit.collider.gameObject.scene);
				placed.transform.position = rayHit.point;
				IPlaceable placeable = placed.GetComponent<IPlaceable> ();
				if (placeable != null) {
					Debug.Log ("PLACEABLE");
					Transform parentObj = ground.root.transform.Find (placeable.path);
					if (parentObj == null) {
						Debug.Log ("CREATE GROUP");
						GameObject newParentObj = new GameObject (placeable.path);
						parentObj = newParentObj.transform;
						parentObj.SetParent (ground.root.transform);
					}
					placed.transform.SetParent (parentObj.transform, true);
					EditorSceneManager.MarkSceneDirty (parentObj.gameObject.scene);

					placeable.Randomize ();
					Selection.activeGameObject = null;
				}
			}
			#endif
		} else if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Tab) {
			SelectedPlaceable = null;
		} 

	}

	#endregion
}
