﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Aluna
{
	public class TypeAssetInspector : Editor
	{
		TypeAsset targetType {
			get {
				return (TypeAsset)target;
			}
		}

		public override Texture2D RenderStaticPreview (string assetPath, Object[] subAssets, int width, int height)
		{
			if (targetType.icon == null)
				return null;
			Texture2D tex = AssetPreview.GetAssetPreview (targetType.icon);
			if (tex == null)
				return null;
			Texture2D chace = new Texture2D (width, height);
			EditorUtility.CopySerialized (tex, chace);
			return chace;
		}

	}
}