﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	[System.Serializable]
	[CreateAssetMenu (menuName = "Item Type")]
	public class ItemType : TypeAsset
	{

		public EffectType InventoryEffect;

	}
}