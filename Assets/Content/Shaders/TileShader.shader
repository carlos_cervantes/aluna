﻿Shader "Unlit/TileShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Randomness( "Random", 2D) = "white" {}
		_Border ( "Border", Range( 0, 10)) = 0.7
		_Noise( "Noise", Range( 0.01, 0.0001) ) = 0.001
		_TextureNoise( "TexNoise", Range( 0, 1 ) ) = 0.5
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 worldPos : TEXCOORD1;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _Randomness;
			float4 _MainTex_ST;
			float _Border;
			float _Noise;
			float _TextureNoise;
			v2f vert (appdata v)
			{
				v2f o;
				o.worldPos = mul( unity_ObjectToWorld, v.vertex );

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float4 fnoise( float2 pos ) {
				float4 n = 0.5 * tex2D( _Randomness, pos );
				n += 0.25 * tex2D( _Randomness, 2 * pos );
				n += 0.25 * tex2D( _Randomness, 4 * pos );
				return n;
			}

			fixed4 frag (v2f i) : SV_Target
			{

				float4 noise = fnoise( _Noise * i.worldPos.xz  );
				noise -= 0.5;
		
				
				float rx = 0.5 * noise.x; 
				float ry = 0.5 * noise.y;

				float2 cUV = 2 * ( i.uv - 0.5);
				float2 b = 4 * clamp (  abs(cUV) - 0.75, - 0.25, 0.25 );
				float border = - max (b.x, b.y );
				float rand = (b.x > b.y ) ? sign(cUV.x) * rx : sign(cUV.y) * ry;
				clip( border - (_Border * rand ) );
				//border += 0.5;
				float4 col = tex2D( _MainTex, i.uv );
				float tr = _TextureNoise * noise.z;
				col = ( 1  - tr) * col + tr * float4( 0, 0, 0, 1);
			//	col *= border;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
