﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : MonoBehaviour, IPlaceable
{
	[SerializeField]
	SpriteRenderer _spriteRenderer;

	public SpriteRenderer spriteRenderer {
		get {
			if (_spriteRenderer == null) {
				_spriteRenderer = GetComponent<SpriteRenderer> ();
			}
			return _spriteRenderer;
		}
	}

	public float CurrentGrowth = 0.0f;
	public float GrowthRate = 0.1f;

	[System.Serializable]
	public class State
	{
		public Sprite sprite;
		public float MinGrowth;
	}

	[SerializeField] 
	public List<State> states = new List<State> ();

	void ApplyCurrentGrowth ()
	{
		Sprite newSprite = states [0].sprite;
		for (int i = 1; i < states.Count; ++i) {
			if (states [i].MinGrowth > CurrentGrowth) {
				break;
			}
			newSprite = states [i].sprite;
		}
		spriteRenderer.sprite = newSprite;
	}


	public string path { 
		get {
			return "Plants";
		}
	}

	public void Ground ()
	{
		CurrentGrowth = Random.value;
	}

	public void Randomize ()
	{
		CurrentGrowth = Random.value;
	}

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		CurrentGrowth += Time.deltaTime * GrowthRate;
		ApplyCurrentGrowth ();
	}
}
