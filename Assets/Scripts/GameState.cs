﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	[System.Serializable]
	public class GameState
	{
		public float time = 0.0f;

		[SerializeField]
		public InventoryContent inventoryContent = new InventoryContent ();

	}
}