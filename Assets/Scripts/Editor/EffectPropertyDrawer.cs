﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Linq;

namespace Aluna
{
	[CustomPropertyDrawer (typeof(Effect))]
	public class EffectPropertyDrawer : PropertyDrawer
	{

		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			if (property.objectReferenceValue == null) {
				return EditorGUIUtility.singleLineHeight;
			} else {
				SerializedObject so = new SerializedObject ((Effect)property.objectReferenceValue);
				SerializedProperty it = so.GetIterator ();
				float height = 0.0f;
				if (it.NextVisible (true)) {
					do {
						height += EditorGUI.GetPropertyHeight (property, true) + EditorGUIUtility.standardVerticalSpacing;
					} while (it.NextVisible (false));
				}
				return height;
			}
		}

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty (position, label, property);
			if (property.objectReferenceValue == null) {
				System.Type selectedType = TypePopup (position);
				if (selectedType != null) {
					Object created = ScriptableObject.CreateInstance (selectedType);
					if (AssetDatabase.Contains (property.serializedObject.targetObject))
						AssetDatabase.AddObjectToAsset (created, property.serializedObject.targetObject);
					property.objectReferenceValue = (Object)created;
				}
			} else {

				Rect pos = position;
				pos.height = EditorGUIUtility.singleLineHeight;
				Rect labelRect = pos;
				labelRect.width -= EditorGUIUtility.singleLineHeight;
				Rect deleteRect = pos;
				deleteRect.x += labelRect.width;
				deleteRect.width = EditorGUIUtility.singleLineHeight;
				EditorGUI.LabelField (labelRect, property.objectReferenceValue.GetType ().ToString (), EditorStyles.boldLabel);
				if (GUI.Button (deleteRect, "X")) {
					Object.DestroyImmediate (property.objectReferenceValue);
					property.objectReferenceValue = null;
				} else { 
					SerializedObject so = new SerializedObject ((Effect)property.objectReferenceValue);
					SerializedProperty it = so.GetIterator ();
					pos.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
					pos.x += EditorGUIUtility.singleLineHeight;
					EditorGUI.BeginChangeCheck ();
					if (it.NextVisible (true)) {
						do {
							if (it.name != "m_Script") {
								EditorGUI.PropertyField (pos, it);
								position.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
							}
						} while (it.NextVisible (false));
					}
					if (EditorGUI.EndChangeCheck ())
						so.ApplyModifiedProperties ();
				}
			}
			EditorGUI.EndProperty ();
		}

		System.Type TypePopup (Rect position)
		{
			List<System.Type> typeList = new List<System.Type> (Assembly.GetAssembly (typeof(Effect)).GetTypes ().Where (t => !t.IsAbstract && t.IsSubclassOf (typeof(Effect))));
			List<string> typeNames = new List<string> ();
			typeNames.Add ("<select effect>");
			typeNames.AddRange (typeList.Select< System.Type, string> (t => t.Name));
			typeList.Insert (0, null);
			int selected = EditorGUI.Popup (position, 0, typeNames.ToArray ());
			return typeList [selected];
		}
	}
}