﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	[System.Serializable]
	public class InventoryContent : SerializableDictionary< ItemType, int >
	{
		public bool IsValid {
			get {
				foreach (int c in  _dictionary.Values) {
					if (c < 0) {
						return false;
					}
				}
				return true;
			}
		}

		public bool IsEmpty {
			get {
				foreach (int c in  _dictionary.Values) {
					if (c > 0) {
						return false;
					}
				}
				return true;
			}
		}

		public void Add (ItemType type, int count = 1)
		{
			if (_dictionary.ContainsKey (type)) {
				_dictionary [type] += count;
			} else {
				_dictionary.Add (type, count);
			}
			if (Changed != null)
				Changed ();
		}

		public bool HasItems (ItemType type, int count = 1)
		{
			if (!_dictionary.ContainsKey (type)) {
				return false;
			} else {
				return _dictionary [type] >= count;
			}
		}

		public bool HasItems (InventoryContent content)
		{
			foreach (KeyValuePair<ItemType, int> entry in content._dictionary) {
				if (HasItems (entry.Key, entry.Value))
					return false;
			}
			return true;
		}





		public void ChangeItems (InventoryContent content)
		{
			foreach (KeyValuePair<ItemType, int> entry in content._dictionary) {
				if (!dictionary.ContainsKey (entry.Key)) {
					dictionary.Add (entry.Key, entry.Value);
				} else {
					dictionary [entry.Key] = Mathf.Max (0, entry.Value + dictionary [entry.Key]);
				}

			}
			if (Changed != null)
				Changed ();
		}


		public int ItemCount (ItemType type)
		{
			int count = 0;
			if (_dictionary.TryGetValue (type, out count))
				return count;
			else
				return 0;
		}

		public event System.Action Changed;
	}
}