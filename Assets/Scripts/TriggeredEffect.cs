﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{

	[System.Serializable]
	public struct TriggeredEffect
	{

		[SerializeField]
		public EventType Trigger;
		[SerializeField] 
		public EffectType Effect;

		public void Check (GameObject contextSubject, EventType evt, GameObject contextObject)
		{
			if (Trigger == evt) {
				Effect.Apply (contextSubject, evt, contextObject);
			}
		}
	}
}