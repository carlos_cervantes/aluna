﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	[System.Serializable]
	public class RemoveFromScene : Effect
	{
	
		[Tooltip ("Specify the name/path of a child element to delte. If left blank the root GameObject of the context ojbect will be deleted.")]
		public string child;

		public override void Apply (GameObject contextSubject, EventType evt, GameObject contextObject)
		{
			GameObject target = contextObject;
			if (contextObject != null && !string.IsNullOrEmpty (child)) {
				Transform found = contextObject.transform.Find (child);
				if (found != null) {
					target = found.gameObject;
				} else {
					Debug.LogWarning ("Could not locate target '" + child + "' to destroy on gameObject " + contextObject);
				}
			}
			GameObject.Destroy (target);
		}
	}
}