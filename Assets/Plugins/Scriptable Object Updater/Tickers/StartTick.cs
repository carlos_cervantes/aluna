﻿using UnityEngine;
using System.Collections;

namespace Lowscope.ScriptableObjectUpdater
{
    public class StartTick : Ticker
    {
        public void Start()
        {
            DispatchTick();
        }
    }
}