using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryListener : MonoBehaviour
{
    [SerializeField]
    private GameObject target;

    private List<Inventory> obtainedInventories = new List<Inventory>();

    private void OnEnable()
    {
        if (target != null)
        {
            Inventory getInventory = target.GetComponent<Inventory>();
            getInventory?.AddListener(this.gameObject);
        }
    }

    private void OnObtainedScriptableReference(GameObject reference)
    {
        Inventory getInventory = reference.GetComponent<Inventory>();
        getInventory?.AddListener(this.gameObject);
        obtainedInventories.Add(getInventory);
    }

    private void OnDisable()
    {
        for (int i = obtainedInventories.Count - 1; i >= 0; i--)
        {
            obtainedInventories[i].RemoveListener(this.gameObject);
            obtainedInventories.Remove(obtainedInventories[i]);
        }
    }
}
