﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlaceable
{
	string path { get; }

	void Ground ();

	void Randomize ();
}
