﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class SerializableDictionary<K, V> : ISerializationCallbackReceiver
{
	[SerializeField]
	private K[] keys;
	[SerializeField]
	private V[] values;


	public Dictionary<K, V> _dictionary;

	public Dictionary<K, V> dictionary {
		get {
			if (_dictionary == null) {
				OnAfterDeserialize ();
			}
			return _dictionary;
		}
	}


	static public T New<T> () where T: SerializableDictionary<K,V>, new()
	{
		var result = new T ();
		result._dictionary = new Dictionary<K, V> ();
		return result;
	}

	public void OnAfterDeserialize ()
	{
		int c = (keys != null) ? keys.Length : 10;
		_dictionary = new Dictionary<K, V> (c);
		if (keys != null && values != null) {
			for (int i = 0; i < c; ++i) {
				if (keys [i] != null)
					_dictionary [keys [i]] = values [i];
			}
		}
		keys = null;
		values = null;
	}

	public void OnBeforeSerialize ()
	{
		if (_dictionary != null) {
			int c = _dictionary.Count;
			keys = new K[c];
			values = new V[c];
			int i = 0;
			using (Dictionary<K,V>.Enumerator e = _dictionary.GetEnumerator ()) {
				while (e.MoveNext ()) {
					KeyValuePair<K, V> kvp = e.Current;
					keys [i] = kvp.Key;
					values [i] = kvp.Value;
					++i;
				}
			}
		} else {
			if (keys == null) {
				keys = new K[0];
				values = new V[0];
			}
		}
	}

}
