﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	[System.Serializable]
	public class TypeAsset : ScriptableObject
	{
		[UnityEngine.Serialization.FormerlySerializedAs ("inventoryIcon")]
		public Sprite icon;

	}
}