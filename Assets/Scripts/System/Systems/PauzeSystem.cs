﻿using UnityEngine;
using System.Collections;
using System;

public class PauzeSystem : GameSystem
{
    [SerializeField]
    private GameEvent pauzeButton;

    [SerializeField]
    private BoolEvent pauzeEvent;

    private bool isPauzed;

    public override void OnLoadSystem()
    {
        pauzeButton.AddListener(OnPauze);
    }

    private void OnPauze ()
    {
        SetPauzed(isPauzed = !isPauzed);
    }

    public void SetPauzed (bool state)
    {
        isPauzed = state;
        pauzeEvent.Invoke(state);
    }
}
