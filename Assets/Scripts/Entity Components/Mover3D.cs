using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody)), DisallowMultipleComponent()]
public class Mover3D : MonoBehaviour, ISaveable
{
    [SerializeField]
    private float speed;

    private List<IMove> IMoveInterfaces = new List<IMove>();

    private Rigidbody rigidBody3D;

    private bool isMovementFrozen;
    public bool IsMovementFrozen { get { return isMovementFrozen; } }

    private void Awake()
    {
        GetComponentsInChildren<IMove>(true, IMoveInterfaces);

        rigidBody3D = GetComponent<Rigidbody>();

        DispatchMoveEvent(Vector3.zero, 0);
    }

    public void Move(Vector3 direction)
    {
        if (isMovementFrozen)
        {
            return;
        }

        direction.Normalize();

        rigidBody3D.MovePosition((Vector3)this.transform.position + ((direction * speed) * Time.deltaTime));

        DispatchMoveEvent(direction, (direction.x == 0 && direction.y == 0) ? 0 : speed);
    }

    public void FreezeMovement(bool state)
    {
        isMovementFrozen = state;

        DispatchMoveEvent(Vector3.zero, 0);
    }

    private void DispatchMoveEvent(Vector3 direction, float speed)
    {
        for (int i = 0; i < IMoveInterfaces.Count; i++)
        {
            IMoveInterfaces[i].OnMove(direction, speed);
        }
    }

    #region Saving

    private Vector3 lastSavedPosition;

    [System.Serializable]
    public struct SaveData
    {
        public Vector3 position;
    }

    public string OnSave()
    {
        lastSavedPosition = this.transform.position;

        return JsonUtility.ToJson(new SaveData()
        {
            position = lastSavedPosition
        });
    }

    public void OnLoad(string data)
    {
        SaveData saveData = JsonUtility.FromJson<SaveData>(data);

        Vector3 newPosition = saveData.position;

        this.transform.position = newPosition;

       
            rigidBody3D.MovePosition(newPosition);
       

        DispatchMoveEvent(Vector3.zero, 0);
        lastSavedPosition = newPosition;
    }

    public bool OnSaveCondition()
    {
        return lastSavedPosition != (Vector3)this.transform.position;
    }

    #endregion
}