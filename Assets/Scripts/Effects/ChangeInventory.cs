﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	[System.Serializable]
	public class ChangeInventory : Effect
	{

		public InventoryContent Items = new InventoryContent ();

		public override void Apply (GameObject contextSubject, EventType evt, GameObject contextObject)
		{
			GameManager.Inventory.ChangeItems (Items);
		}
	}
}