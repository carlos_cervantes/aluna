﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Aluna
{
	[CustomPropertyDrawer (typeof(InventoryContent))]
	public class InventoryContentPropertyDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{

			SerializedProperty Keys = property.FindPropertyRelative ("keys");
			return (Keys.arraySize + 2) * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);

		}

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty (position, label, property);
			SerializedProperty Keys = property.FindPropertyRelative ("keys");
			SerializedProperty Values = property.FindPropertyRelative ("values");

			Rect pos = position;
			pos.height = EditorGUIUtility.singleLineHeight;

			EditorGUI.LabelField (pos, label);
			pos.x += EditorGUIUtility.singleLineHeight;
			pos.width -= EditorGUIUtility.singleLineHeight;
			int ToRemove = -1;
			EditorGUI.BeginChangeCheck ();
			for (int i = 0; i != Keys.arraySize; ++i) {
				pos.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

				Rect TypePos = pos;
				TypePos.width = 0.4f * pos.width;
				Rect CountPos = pos;
				CountPos.x += TypePos.width;
				CountPos.width = 0.4f * pos.width;
				Rect RemoveButtonPos = pos;
				RemoveButtonPos.width = 0.2f * pos.width;
				RemoveButtonPos.x += TypePos.width + CountPos.width;

				EditorGUI.PropertyField (TypePos, Keys.GetArrayElementAtIndex (i), GUIContent.none);
				EditorGUI.PropertyField (CountPos, Values.GetArrayElementAtIndex (i), GUIContent.none);
				if (GUI.Button (RemoveButtonPos, "X")) {
					ToRemove = i;
				}
			}
			pos.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

			if (ToRemove >= 0) {
				Keys.DeleteArrayElementAtIndex (ToRemove);
				Values.DeleteArrayElementAtIndex (ToRemove);
			}
			ItemType toAdd = null;
			toAdd = (ItemType)EditorGUI.ObjectField (pos, "Add", toAdd, typeof(ItemType), false);
			if (toAdd != null) {
				Debug.Log ("Adding " + toAdd);
				Values.arraySize++;
				Keys.arraySize++;
				Keys.GetArrayElementAtIndex (Keys.arraySize - 1).objectReferenceValue = toAdd;
				Values.GetArrayElementAtIndex (Values.arraySize - 1).intValue = 1;

			}
			if (EditorGUI.EndChangeCheck () || ToRemove >= 0 || toAdd != null)
				property.serializedObject.ApplyModifiedProperties ();
		
			EditorGUI.EndProperty ();
		}
	}
}