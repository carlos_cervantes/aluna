﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Events/Quaternion Event")]
public class QuaternionEvent : ScriptableEvent<Quaternion>
{
    public void Invoke()
    {
        
    }
}
