﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	public class PlayerCharacter : MonoBehaviour
	{
		public static PlayerCharacter Instance;

		void Awake ()
		{
			Instance = this;
		}

		public float speed = 1.0f;
		public string WalkingAnimatorBool = "Walking";
		public bool m_FacingRight = true;

		public GameObject frontView;
		public GameObject backView;
		public GameObject sideView;

		GameObject _activeView;

		public GameObject activeView {
			get {
				return _activeView;
			}
			set {
				_activeView = value;
				frontView.SetActive (_activeView == frontView);
				backView.SetActive (_activeView == backView);
				sideView.SetActive (_activeView == sideView);
			}
		}

		[Header ("Components")]
		[SerializeField]
		Animator _animator;

		public Animator animator {
			get {
				if (_animator == null)
					_animator = GetComponentInChildren<Animator> ();
				return _animator;
			}
		}

		[SerializeField]
		CharacterController _characterController;



		public CharacterController characterController {
			get {
				if (_characterController == null)
					_characterController = GetComponent<CharacterController> ();
				return _characterController;
			}
		}

		void FixedUpdate ()
		{

			float deltaX = Input.GetAxis ("Horizontal") * speed;
			float deltaZ = Input.GetAxis ("Vertical") * speed;
			Vector3 movement = new Vector3 (deltaX, 0, deltaZ);
			movement = Vector3.ClampMagnitude (movement, speed); //Limit diagonal movement to the same speed as mov along axis.
			movement *= Time.deltaTime;
			movement = transform.TransformDirection (movement);
			characterController.Move (movement);
			float input_x = Input.GetAxis ("Horizontal");
			float input_y = Input.GetAxis ("Vertical");
			if (animator != null) {
				animator.SetBool (WalkingAnimatorBool, characterController.velocity.magnitude > 0.0f);
				animator.SetFloat ("X", input_x);
				animator.SetFloat ("Y", input_y);
            
				if (input_y > 0) {
					animator.SetFloat ("LastMoveY", 1);
					animator.SetFloat ("LastMoveX", 0);
					activeView = backView;
				}
				if (input_y < 0) {
					animator.SetFloat ("LastMoveY", -1);
					animator.SetFloat ("LastMoveX", 0);
					activeView = frontView;
				}
				if (input_x > 0 && !m_FacingRight) {
					animator.SetFloat ("LastMoveX", 1);
					animator.SetFloat ("LastMoveY", 0);
					activeView = sideView;
					Flip ();

				}
				if (input_x < 0 && m_FacingRight) {
					animator.SetFloat ("LastMoveX", -1);
					animator.SetFloat ("LastMoveY", 0);
					activeView = sideView;
					Flip ();
				}
			}
		}

        private void Flip ()
		{
			// Switch the way the player is labelled as facing.
			m_FacingRight = !m_FacingRight;

			// Multiply the player's x local scale by -1.
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}

	}
}