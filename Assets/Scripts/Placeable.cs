﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placeable : MonoBehaviour, IPlaceable
{

	[SerializeField]
	string _path;

	public string path {
		get {
			return _path;
		}
	}

	public bool RandomnizeScale = false;
	public float MinScale = 0.9f;
	public float MaxScale = 1.1f;

	public void Ground ()
	{

	}

	public void Randomize ()
	{
		if (RandomnizeScale)
			transform.localScale = Random.Range (MinScale, MaxScale) * Vector3.one;
	}
}