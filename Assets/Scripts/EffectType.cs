﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	[System.Serializable]
	[CreateAssetMenu (menuName = "Effect")]
	public class EffectType : TypeAsset
	{

		[SerializeField]
		public List<Effect> Effects = new List<Effect> ();

		public void Apply (GameObject contextSubject, EventType evt, GameObject contextObject)
		{
			foreach (Effect e in Effects)
				if (e != null)
					e.Apply (contextSubject, evt, contextObject);
		}
	}
}