﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{
	[System.Serializable]
	public abstract class Effect : ScriptableObject
	{
		public abstract void Apply (GameObject contextSubject, EventType evt, GameObject contextObject);
	}
}