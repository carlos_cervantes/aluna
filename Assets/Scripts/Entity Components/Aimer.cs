using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Used to define the current aim direction.
/// Player mouse cursor aiming tools will interact with this component.
/// </summary>
[AddComponentMenu("Aluna/Entity Components/Movement/Aimer")]
public class Aimer : MonoBehaviour, IMove, ISaveable
{
    private List<IAim> IAimInterfaces = new List<IAim>();

    private Vector3 lastAimDirection;

    private void Awake()
    {
        GetComponentsInChildren<IAim>(true, IAimInterfaces);
    }

    public void OnMove(Vector3 direction, float velocity)
    {
        if (direction != Vector3.zero)
        {
            lastAimDirection = direction;
            isDirty = true;
        }
    }

    public Vector3 GetAimDirection()
    {
        return lastAimDirection;
    }

    public Vector3 GetAimPosition()
    {
        return (Vector3)transform.position + (GetAimDirection() * 0.10f);
    }

    public void LookAt(Vector3 target)
    {
        Vector3 lookVector = (target - (Vector3)this.transform.position);

        if (Mathf.Abs(lookVector.x) > Mathf.Abs(lookVector.z))
        {
            lookVector.z = 0;

            if (lookVector.x > 0)
            {
                lookVector.x = 1;
            }
            else
            {
                lookVector.x = -1;
            }
        }
        else
        {
            lookVector.x = 0;

            if (lookVector.z > 0)
            {
                lookVector.z = 1;
            }
            else
            {
                lookVector.z = -1;
            }
        }

        SetAimDirection(lookVector);
    }

    public void SetAimDirection(Vector3 direction)
    {
        isDirty = true;
        IAimInterfaces.ForEach((_interface) => _interface.OnAim(direction));
        lastAimDirection = direction;
    }

    #region Saving

    [System.Serializable]
    public struct RuntimeData
    {
        public Vector3 aimDirection;
    }

    public string OnSave()
    {
        return JsonUtility.ToJson(new RuntimeData() { aimDirection = lastAimDirection });
    }

    public void OnLoad(string data)
    {
        RuntimeData getData = JsonUtility.FromJson<RuntimeData>(data);

        lastAimDirection = getData.aimDirection;
        SetAimDirection(getData.aimDirection);
        IAimInterfaces.ForEach((_interface) => _interface.OnAim(getData.aimDirection));
    }

    private bool isDirty = false;

    public bool OnSaveCondition()
    {
        if (isDirty)
        {
            isDirty = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion
}