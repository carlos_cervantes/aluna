﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{

	public class InventoryUI : MonoBehaviour
	{

		[SerializeField]
		GameObject _ContainerUIElement;

		public GameObject ContainerUIElement {
			get {
				if (_ContainerUIElement == null)
					return gameObject;
				else
					return _ContainerUIElement;
			}
		}

		public InventoryEntry prefab;
		Dictionary<ItemType, InventoryEntry> existingEntries = new Dictionary<ItemType, InventoryEntry> ();

		void Start ()
		{
			UpdateUI ();
			GameManager.Inventory.Changed += UpdateUI;
		}

		void OnDestroy ()
		{
			GameManager.Inventory.Changed -= UpdateUI;
		}

		public void UpdateUI ()
		{
			foreach (KeyValuePair<ItemType, int> entry in GameManager.Inventory.dictionary) {
				if (entry.Value > 0) {
					if (!existingEntries.ContainsKey (entry.Key)) {
						GameObject spawned = (GameObject)Instantiate (prefab.gameObject, ContainerUIElement.transform, false);
						InventoryEntry ie = spawned.GetComponent<InventoryEntry> ();
						ie.IconImage.sprite = entry.Key.icon;
						ie.CountLabel.text = entry.Value.ToString ();
						ie.Type = entry.Key;
						existingEntries.Add (entry.Key, ie);
					} else {
						existingEntries [entry.Key].CountLabel.text = entry.Value.ToString ();
					}
				} else {
					Destroy (existingEntries [entry.Key].gameObject);
					existingEntries.Remove (entry.Key);
				}
			}
		}
	}
}