﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aluna
{

	public class Interactable : MonoBehaviour
	{
		public static HashSet<Interactable> InRange = new HashSet<Interactable> ();


		public EventType PlayerEnterEvent;
		public EventType PlayerExitEvent;

		public List<TriggeredEffect> behaviours = new List<TriggeredEffect> ();

		public void HandleEvent (GameObject contextSubject, EventType evt)
		{
			foreach (TriggeredEffect behaviour in behaviours) {
				behaviour.Check (contextSubject, evt, gameObject);
			}
		}


		void OnTriggerEnter (Collider col)
		{
			if (col.gameObject == PlayerCharacter.Instance.gameObject) {
				InRange.Add (this);
				if (PlayerEnterEvent)
					this.HandleEvent (PlayerCharacter.Instance.gameObject, PlayerEnterEvent);
			}
		}

		void OnTriggerExit (Collider col)
		{
			if (col.gameObject == PlayerCharacter.Instance.gameObject) {
				if (PlayerExitEvent)
					this.HandleEvent (PlayerCharacter.Instance.gameObject, PlayerExitEvent);
				InRange.Remove (this);
			}
		}

		void OnDestroy ()
		{
			InRange.Remove (this);
		}
	}
}