﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Aluna
{
	public class StaminaBar : MonoBehaviour
	{

		public Image Bar;
	
		// Update is called once per frame
		void Update ()
		{
			Bar.fillAmount = GameManager.CurrentStamina / GameManager.MaxStamina;
		}
	}
}