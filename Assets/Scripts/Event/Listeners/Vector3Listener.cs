﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Vector3Listener : ScriptableEventListener<Vector3>
{
    public Vector3Event eventObject;

    public UnityEventVector3 eventAction = new UnityEventVector3();

    protected override ScriptableEvent<Vector3> ScriptableEvent
    {
        get
        {
            return eventObject;
        }
    }

    protected override UnityEvent<Vector3> Action
    {
        get
        {
            return eventAction;
        }
    }
}
