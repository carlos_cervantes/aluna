﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Aluna
{
	public class DebugInformation : EditorWindow
	{

		[MenuItem ("Aluna/Debug Information")]
		public static void Init ()
		{
			GetWindow<DebugInformation> ().Show ();
		}

		void OnGUI ()
		{
			EditorGUILayout.LabelField ("Interactables in Range");
			EditorGUI.indentLevel++;
			foreach (Interactable i in Interactable.InRange) {
				EditorGUILayout.LabelField ((i != null) ? i.name : "<null>");
			}
			EditorGUI.indentLevel--;
		}


		void Update ()
		{
			if (Application.isPlaying)
				Repaint ();
		}

	}
}