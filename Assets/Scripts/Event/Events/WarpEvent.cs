﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu(menuName = "Events/Warp Event")]
public class WarpEvent : ScriptableEvent<WarpLocation>
{

}
