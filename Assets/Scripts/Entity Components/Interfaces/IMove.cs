﻿using UnityEngine;
using System.Collections;

public interface IMove
{
    void OnMove(Vector3 direction, float velocity);
}
