﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Aluna
{
	public class InventoryEntry : MonoBehaviour
	{

		public Image IconImage;
		public Text CountLabel;
		public Button ApplyButton;

		public ItemType Type;

		public void HandleClick ()
		{
			if (Type != null && Type.InventoryEffect != null) {
				Type.InventoryEffect.Apply (PlayerCharacter.Instance.gameObject, null, null);
			}
		}

		void Start ()
		{
			ApplyButton.onClick.AddListener (HandleClick);
		}
	}
}