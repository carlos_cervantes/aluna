﻿
using UnityEngine;

public interface IAim
{
    void OnAim(Vector3 direction);
}