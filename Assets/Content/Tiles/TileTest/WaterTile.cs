﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class WaterTile : TileBase
{
	public Sprite[] sprites;
	public float speed = 1.0f;
	public float startTime = 0.0f;

	public override void GetTileData (Vector3Int position, ITilemap tilemap, ref TileData tileData)
	{
		tileData.sprite = sprites [0];
	}

	public override bool GetTileAnimationData (Vector3Int position, ITilemap tilemap, ref TileAnimationData tileAnimationData)
	{
		tileAnimationData.animatedSprites = sprites;
		tileAnimationData.animationSpeed = speed;
		tileAnimationData.animationStartTime = (Mathf.Abs (position.x + position.y) % 2 > 0) ? 0.0f : 1.0f;
		return true;
	}
}
