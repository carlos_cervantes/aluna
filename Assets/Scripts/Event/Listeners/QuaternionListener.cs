﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class QuaternionListener : ScriptableEventListener<Quaternion>
{
    public QuaternionEvent eventObject;

    public UnityEventQuaternion eventAction = new UnityEventQuaternion();

    protected override ScriptableEvent<Quaternion> ScriptableEvent
    {
        get
        {
            return eventObject;
        }
    }

    protected override UnityEvent<Quaternion> Action
    {
        get
        {
            return eventAction;
        }
    }
}
